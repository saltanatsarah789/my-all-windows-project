Feature:  LogIn Action Functionality
  Description: This feature will test a LogIn and LogOut functionality

  Background:
    Given As a not validated user
    When  User navigate to the url "https://www.barnesandnoble.com/"
    Then  BarnesAndNoble Home Page should show
    When  User click on SignIn link

  @debug
  @smoke
  Scenario: 1.Valid User Email and valid User Password
    And   User click with "mahbub80@gmail.com" as Email
    And   User click with "Mahbub123" as Password
    And   Click SignIn
    Then Welcome message shows "Welcome"

  @smoke
  Scenario: 2.InValid User valid Email invalid Password
    And User click "mahbub80@gmail.com" as Email
    And User click "Newyear2018" as Password
    And Click SignIn
    Then Error Message should display

  @debug
  Scenario: 3.InValid User invalid Email valid Password
    And User click "mahbub180@gggg.com" as Email
    And User click "Mahbub123" as Password
    And Click SignIn
    Then Error Message should display

  @smoke
  Scenario: 4.InValid User Email and invalid User Password
    And   Login with user Email "mah444@gmail.com" and Password "invalid4444"
    And   Click SignIn
    Then Error Message should display