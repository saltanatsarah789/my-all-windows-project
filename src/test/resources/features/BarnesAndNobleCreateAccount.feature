Feature: Create Account Action Functionality
  Description: This feature will test a create account functionality

  Background:
    Given As a not validated user
    When  User navigate to the url "https://www.barnesandnoble.com/"
    Then  BarnesAndNoble Home Page should show
    When  User click on SignIn link

  @debug
  @smoke
  Scenario: 1.Create a new User
    And   User click CreateAnAccount
    And   Enter "Mahbub" as FirstName
    And   Enter "Hossain" as LastName
    And   Enter "mahbub80@gmail.com" as Email
    And   Enter "mahbub80@gmail.com" as ConfirmEmail
    And   Enter "Mahbub123" as Password
    And   Enter "Mahbub123" as ConfirmPassword
    And   User click Security Question
    And   Enter "QA Tester" Security Answer
    And   User click Create Account
    Then Welcome message shows "Welcome"


