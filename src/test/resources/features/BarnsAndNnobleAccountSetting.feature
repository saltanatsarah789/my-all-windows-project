Feature:  My account Action Functionality
  Description: This feature will test a AccountSetting functionality


  Background:
    Given As a not validated user
    When  User navigate to the url "https://www.barnesandnoble.com/"
    Then  BarnesAndNoble Home Page should show
    When  User click MyAccount
    When Valid user click Manage Account

  @debug1234
  Scenario: 1.User can edit Account Setting
    When Valid user click Manage Account Setting
    And Valid user click "mahbub800@gmail.com" as Email
    And Valid user click "Mahbub123" as Password
    And User click Secure Sign In
    When Verifay Account Setting page
    And User can click change name "Abid" as FName
    And User can click change name "Rahman" as LName
    And User can click Save Changes

  Scenario: 2. User can Change Email Address
    When Valid user click Manage Account Setting
    And Valid user click "mahbub80@gmail.com" as Email
    And Valid user click "Mahbub123" as Password
    And User click Secure Sign In
    When Verifay Account Setting page
    And User see "mahbub80@gmail.com" as current Email
    And User click "mah444bub@shiftedtech.com" as New Email
    And User click "mah444bub@shiftedtech.com" as New Email Cornfirm
    And User can click Save Changes
