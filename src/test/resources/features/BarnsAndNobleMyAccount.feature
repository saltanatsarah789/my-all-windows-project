Feature:  My account Action Functionality
  Description: This feature will test a myAccount functionality

  Background:
    Given As a not validated user
    When  User navigate to the url "https://www.barnesandnoble.com/"
    Then  BarnesAndNoble Home Page should show
    When  User click MyAccount

    @smoke
    Scenario: 1.User can check MyAccount
       And  Valid user click Manage Account
       And  User click with "mahbub80@gmail.com" as Email Order History
       And  User click with "12345" as Order Number Order History
       And  Click Find Button
       Then Error Order History message display "Sorry, this order number and email address don't match our records. Please try again."

      @debug123
      Scenario: 2.User can check MyAccount
        And  Valid user click Manage Account
        And  Valid user click Address Book
        Then User click "mahbub80@gmail.com" as Email
        And  User click "Mahbub123" as Password
        And  User click Secure SignIn link






