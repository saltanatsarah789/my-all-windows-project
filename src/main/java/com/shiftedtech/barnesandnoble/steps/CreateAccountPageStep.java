package com.shiftedtech.barnesandnoble.steps;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class CreateAccountPageStep extends StepBase{

    @When("^User click on SignIn link$")
    public void user_click_on_SignIn_link() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@class='sign-in-link']"));
        highlight(element);
        element.click();
        Thread.sleep(2000);

    }


    @When("^User click CreateAnAccount$")
    public void user_click_CreateAnAccount() throws Throwable {
        switchToIfrmae("loginFrame");
        WebElement element = driver.findElement(By.xpath("//a[@id='createAccountBtn']"));
        highlight(element);
        element.click();
        delayFor(3000);
    }

    @When("^Enter \"([^\"]*)\" as FirstName$")
    public void enter_as_FirstName(String fName) throws Throwable {
        driver.switchTo().defaultContent();
        switchToIfrmae("registerFrame");
        WebElement element = driver.findElement(By.xpath("//*[@id='fName']"));
        element.sendKeys(fName);
    }

    @When("^Enter \"([^\"]*)\" as LastName$")
    public void enter_as_LastName(String lName) throws Throwable {
        WebElement element = driver.findElement(By.id("lName"));
        element.sendKeys(lName);
    }

    @When("^Enter \"([^\"]*)\" as Email$")
    public void enter_as_Email(String eMail) throws Throwable {
        WebElement element = driver.findElement(By.id("email"));
        element.sendKeys(eMail);

    }

    @When("^Enter \"([^\"]*)\" as ConfirmEmail$")
    public void enter_as_ConfirmEmail(String cEmail) throws Throwable {
        WebElement element = driver.findElement(By.id("confirmEmail"));
        element.sendKeys(cEmail);
    }

    @When("^Enter \"([^\"]*)\" as Password$")
    public void enter_as_Password(String pword) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='password']"));
        element.sendKeys(pword);
    }

    @When("^Enter \"([^\"]*)\" as ConfirmPassword$")
    public void enter_as_ConfirmPassword(String pword) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='confPassword']"));
        element.sendKeys(pword);
    }

    @When("^User click Security Question$")
    public void user_click_Security_Question() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='securityQuestion-replacement']"));
        element.click();

        List<WebElement> options = element.findElements(By.xpath("./../ul/li/a"));
        System.out.println("Total List: " + options.size());
        for (WebElement item : options) {
            System.out.println(item.getText());
            if(item.getText().contentEquals("What is your dream job?")){
                item.click();
                break;
            }
        }
    }

    @When("^Enter \"([^\"]*)\" Security Answer$")
    public void enter_Security_Answer(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.id("securityAnswer"));
        element.sendKeys(arg1);
    }

    @When("^User click Create Account$")
    public void user_click_Create_Account() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='btnCreateAccount']"));
        jsClickEx(element);
    }

    @Then("^Welcome message shows \"([^\"]*)\"$")
    public void welcome_message_shows(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='userLinks']/a"));
        String actual = element.getText();
        String expected = "Welcome, Mahbub";
        Assert.assertEquals(expected,actual);

    }

    public void delayFor(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
