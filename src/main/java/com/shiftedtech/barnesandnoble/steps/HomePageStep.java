package com.shiftedtech.barnesandnoble.steps;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class HomePageStep extends StepBase {

    @Given("^As a not validated user$")
    public void as_a_not_validated_user() throws Throwable {
        driver.manage().deleteAllCookies();
    }

    @When("^User navigate to the url \"([^\"]*)\"$")
    public void user_navigate_to_the_url(String arg1) throws Throwable {
        //System.out.println("By link"+arg1);
        driver.navigate().to(arg1);
    }

    @Then("^BarnesAndNoble Home Page should show$")
    public void barnesandnoble_Home_Page_should_show() throws Throwable {
        WebElement popupDialounge = null;
        try {
            popupDialounge = driver.findElement(By.xpath("//div[@role='dialog']"));
            if (popupDialounge != null){
                WebElement closeButton = popupDialounge.findElement(By.xpath("//a[@class='icon-close-modal']"));
                highlight(closeButton);
                closeButton.click();
            }
        }catch (Exception ex){
            System.out.println("Script Continue ...");
        }

        Thread.sleep(3000);

        WebElement element = driver.findElement(By.cssSelector("title"));
        String text = element.getAttribute("text");
        Assert.assertEquals("Online Bookstore: Books, NOOK ebooks, Music, Movies & Toys | Barnes & Noble®",text);

    }


}
