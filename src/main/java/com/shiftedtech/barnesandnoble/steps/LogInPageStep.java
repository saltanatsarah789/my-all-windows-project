package com.shiftedtech.barnesandnoble.steps;

import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.junit.Assert;

import java.util.List;

public class LogInPageStep extends StepBase {

    @When("^User click with \"([^\"]*)\" as Email$")
    public void user_click_with_as_Email(String arg1) throws Throwable {
        numberOfIFrames();
        switchToIfrmae("loginFrame");
        WebElement element = driver.findElement(By.xpath("//input[@id='email']"));
        highlight(element);
        element.sendKeys(arg1);

    }

   @When("^User click with \"([^\"]*)\" as Password$")
    public void user_click_with_as_Password(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='password']"));
        highlight(element);
        element.sendKeys(arg1);
    }

    @When("^Click SignIn$")
    public void click_SignIn() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//button[@class='btn btn--large']"));
        highlight(element);
        element.click();
        Thread.sleep(200);
        }

    @When("^Welcome message Shows$")
    public void welcome_message_Shows() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='userLinks']/a"));
        String actual = element.getText();
        String expected = "Welcome, Mahbub";
        System.out.println("Expected: " + actual);
        Assert.assertEquals(expected,actual);


    }



    @When("^User click \"([^\"]*)\" as Email$")
    public void user_click_as_Email(String arg1) throws Throwable {
      //  numberOfIFrames();
        switchToIfrmae("loginFrame");
        WebElement element = driver.findElement(By.xpath("//*[@id='email']"));
        element.sendKeys(arg1);
    }



    @When("^User click \"([^\"]*)\" as Password$")
    public void user_click_as_Password(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='password']"));
        element.sendKeys(arg1);
    }



    @When("^User click on SignIn$")
    public void user_click_on_SignIn() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//button[@class='btn btn--large']"));
        element.click();
    }

    @When("^Login with user Email \"([^\"]*)\" and Password \"([^\"]*)\"$")
    public void login_with_user_Email_and_Password(String email, String password) throws Throwable {
        user_click_with_as_Email(email);
        user_click_with_as_Password(password);
        click_SignIn();

        }


    @When("^Error Message should display$")
    public void show_message_password_does_not_match() throws Throwable {
        WebElement element = driver.findElement(By.xpath("#mloginErrors"));
        Assert.assertEquals("Your email and password combination does not match our records. Please try again.", element.getText());
        Thread.sleep(1000);
        System.out.println("There is one more error on the screen :- " +element.getText());
    }




}
