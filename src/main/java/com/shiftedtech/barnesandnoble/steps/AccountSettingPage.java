package com.shiftedtech.barnesandnoble.steps;


import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class AccountSettingPage extends StepBase {

    @When("^Valid user click Manage Account Setting$")
    public void valid_user_click_Manage_Account_Setting() throws Throwable {
        WebElement element = driver.findElement(By.xpath("html/body/main/main/div[1]/div/div[2]/div/div[2]/section[1]/div/section/div[2]/a"));
        highlight(element);
        element.click();
    }

    @When("^Valid user click \"([^\"]*)\" as Email$")
    public void valid_user_click_as_Email(String arg1) throws Throwable {
        switchToIfrmae("loginFrame");
        WebElement element = driver.findElement(By.xpath("//*[@id='email']"));
        element.sendKeys(arg1);

    }

    @When("^Valid user click \"([^\"]*)\" as Password$")
    public void valid_user_click_as_Password(String arg1) throws Throwable {
        WebElement pass = driver.findElement(By.xpath("//*[@id='password']"));
        highlight(pass);
        pass.sendKeys(arg1);

    }

    @When("^User click Secure Sign In$")
    public void user_click_Secure_Sign_In() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//button[@class='btn btn--large']"));
        highlight(element);
        element.click();
        Thread.sleep(200);

    }

    @When("^Verifay Account Setting page$")
    public void verifay_Account_Setting_page() throws Throwable {
        WebElement element = driver.findElement(By.cssSelector(".rule.pb-s"));
        String expected = element.getText();
        System.out.println(expected);
        Assert.assertEquals("Account Settings", expected);
    }

    @When("^User can click change name \"([^\"]*)\" as FName$")
    public void user_can_click_change_name_as_FName(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='fName']"));
        element.clear();
        delayFor(200);
        element.sendKeys(arg1);
        delayFor(200);
    }

    @When("^User can click change name \"([^\"]*)\" as LName$")
    public void user_can_click_change_name_as_LName(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='lName']"));
        element.clear();
        delayFor(200);
        element.sendKeys(arg1);
        Thread.sleep(200);

    }

    @When("^User can click Save Changes$")
    public void user_can_click_Save_Changes() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//input[@value='Save Changes']"));
        element.click();
        delayFor(200);
    }


    @When("^User see \"([^\"]*)\" as current Email$")
    public void user_see_as_current_Email(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='current-email-container']"));
        delayFor(300);
        }

    @When("^User click \"([^\"]*)\" as New Email$")
    public void user_click_as_New_Email(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='newEmail']"));
        element.sendKeys(arg1);
        delayFor(300);

    }

    @When("^User click \"([^\"]*)\" as New Email Cornfirm$")
    public void user_click_as_New_Email_Cornfirm(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='confNewEmail']"));
        element.sendKeys(arg1);
        delayFor(300);

    }

}
