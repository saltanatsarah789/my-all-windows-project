package com.shiftedtech.barnesandnoble.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class StepBase {

    protected static WebDriver driver;

   // protected  WebDriver driver = DriverFactory.getInstance().getDriver();;

    public void numberOfIFrames(){
        //By executing a java script
     /*   JavascriptExecutor exe = (JavascriptExecutor) driver;
        Integer numberOfFrames = Integer.parseInt(exe.executeScript("return window.length").toString());
        System.out.println("Number of iframes on the page are " + numberOfFrames);

        //By finding all the web elements using iframe tag
        List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
           System.out.println("The total number of iframes are " + iframes.size());
        for(WebElement iframe : iframes){
            String iframeName = iframe.getAttribute("name");
            System.out.println("IframeName" + iframeName);
            String iframeId = iframe.getAttribute("id");
            System.out.println("IframeId" + iframeId);*/



    }




    public void switchToIfrmae(String str) {
        if (str.contentEquals("loginFrame")) {
            List<WebElement> iframeList = driver.findElements(By.tagName("iframe"));
            System.out.println("Number of Iframe: " + iframeList.size());

            for (WebElement ifamre : iframeList) {
                String src = ifamre.getAttribute("src");
                System.out.println("Src" + src);
                if (src.contains("/www.barnesandnoble.com/account/login-frame-ra")) {
                    System.out.println("Iframe Found");
                    driver.switchTo().frame(ifamre);
                    break;
                }
            }
        }
        if (str.contentEquals("registerFrame")){
            List<WebElement> iframeList = driver.findElements(By.tagName("iframe"));
            System.out.println("Number of Iframe: " + iframeList.size());

            for(WebElement ifamre : iframeList){
                String src = ifamre.getAttribute("src");
                System.out.println("Src" + src);
                if(src.contains("/www.barnesandnoble.com/account/register-frame-ra")){
                    System.out.println("Iframe Found");
                    driver.switchTo().frame(ifamre);
                    break;
                }
            }
        }
    }



    protected void highlight(WebElement element) {
        for (int i = 0; i < 2; i++) {
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "border: 2px solid red;");
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    element, "");
            }
    }

    public void jsClickEx(WebElement element){
        ((JavascriptExecutor) driver).executeScript("var el=arguments[0]; setTimeout(function() { el.click(); }, 100);",  element);
    }

    public void scrollElementIntoView(WebElement element){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("argument[0].scrollIntoView(true);", element);
    }

    protected void scrollToElement(WebElement element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        delayFor(3000);
    }


    public void delayFor ( int time){

        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}
