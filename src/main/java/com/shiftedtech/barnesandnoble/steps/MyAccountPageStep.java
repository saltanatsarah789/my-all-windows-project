package com.shiftedtech.barnesandnoble.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.junit.Assert;
import java.util.List;


public class MyAccountPageStep extends StepBase {

    @When("^User click MyAccount$")
    public void user_click_MyAccount() throws Throwable {
        Actions actions= new Actions(driver);
        WebElement element = driver.findElement(By.linkText("My Account"));
        element.click();
        actions.moveToElement(element);
        actions.click().perform();
        Thread.sleep(200);
        }


    @When("^Valid user click Manage Account$")
    public void valid_user_click_Manage_Account() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='myAccountLinks-old']/li[3]/a"));
        highlight(element);
        element.click();
        delayFor(200);
    }

    @When("^User click with \"([^\"]*)\" as Email Order History$")
    public void user_click_with_as_Email_Order_History(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='email-address']"));
        highlight(element);
        element.sendKeys(arg1);
        delayFor(200);

    }

    @When("^User click with \"([^\"]*)\" as Order Number Order History$")
    public void user_click_with_as_Order_Number_Order_History(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='order-number']"));
        element.sendKeys(arg1);
        delayFor(200);

    }

    @When("^Click Find Button$")
    public void click_Find_Button() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='submit']"));
        element.click();
        delayFor(200);

        }

    @Then("^Error Order History message display \"([^\"]*)\"$")
    public void error_Order_History_message_display(String arg1) throws Throwable {
      WebElement element = driver.findElement(By.xpath("//em[@class='emphasis emphasis--alert']"));
      String text = element.getText();
      String actual = text.replaceAll("\"", "" );
      System.out.println("Actual: " + text);
      System.out.println("ActualActual1212121212: " + actual);
      Assert.assertEquals(arg1,actual);
    }


    @Then("^My Account message should be show$")
    public void my_Account_message_should_be_show() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@class='rule']"));
        highlight(element);
        Assert.assertEquals("My Account",element.getText());
        System.out.println("AHGFHDFH: " + element.getText());
    }



    }

