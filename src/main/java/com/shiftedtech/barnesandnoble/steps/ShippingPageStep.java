package com.shiftedtech.barnesandnoble.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.testng.Assert.assertEquals;

public class ShippingPageStep extends StepBase {



    @When("^Valid user click Address Book$")
    public void valid_user_click_Address_Book() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//a[@class='btn']"));
        highlight(element);
        element.click();
        delayFor(200);
    }

    @Then("^User click Secure SignIn link$")
    public void user_click_Secure_SignIn_link() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//button[@class='btn btn--large']"));
        highlight(element);
        element.click();
        Thread.sleep(200);
    }

    @When("^Valid user click add New Shipping address$")
    public void valid_user_click_add_New_Shipping_address() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='addNewShippingAddress']"));
        highlight(element);
        element.click();
        delayFor(200);

    }

    @When("^User click Country as country$")
    public void user_click_Country_as_country() throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='country-replacement']"));
        highlight(element);
        delayFor(200);

        }

    @When("^User click FName \"([^\"]*)\" as firstName$")
    public void user_click_FName_as_firstName (String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='firstName']"));
        highlight(element);
        element.sendKeys(arg1);
        Thread.sleep(100);
    }

    @When("^User click LName \"([^\"]*)\" as lastName$")
    public void user_click_LName_as_lastName (String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='lastName']"));
        highlight(element);
        element.sendKeys(arg1);
        delayFor(200);
    }

    @When("^User click St Address \"([^\"]*)\" as Street Adress$")
    public void user_click_St_Address_as_Street_Adress (String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='streetAddress']"));
        highlight(element);
        element.sendKeys(arg1);
        delayFor(200);
    }

    @Then("^User click #Num \"([^\"]*)\" as Apt Number$")
    public void user_click_Num_as_Apt_Number(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='aptSuite']"));
        highlight(element);
        element.sendKeys(arg1);
        delayFor(200);
    }

    @When("^User click city \"([^\"]*)\" as City$")
    public void user_click_city_as_City (String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='city']"));
        highlight(element);
        element.sendKeys(arg1);
        delayFor(200);
    }

    @When("^User click State as State$")
    public void user_click_State_as_State () throws Throwable {
        WebElement element = driver.findElement(By.xpath("//*[@id='state-replacement']"));
        element.click();
        List<WebElement> options = element.findElements(By.xpath("./../ul/li/a"));
        System.out.println("Total List: " + options.size());
        for (WebElement item : options) {
            System.out.println(item.getText());
            if (item.getText().contentEquals("New York")) {
                delayFor(3000);
                item.click();
                break;
            }
        }

    }


        @When("^User click zip \"([^\"]*)\" as Zip Code$")
        public void user_click_zip_as_Zip_Code (String arg1) throws Throwable {
            WebElement element = driver.findElement(By.xpath("//*[@id='zipCode']"));
            highlight(element);
            element.sendKeys(arg1);
            delayFor(200);
        }

        @When("^User click PhNum \"([^\"]*)\" as PhoneNumber$")
        public void user_click_PhNum_as_PhoneNumber (String arg1) throws Throwable {
            WebElement element = driver.findElement(By.xpath("//*[@id='phoneNumber']"));
            highlight(element);
            element.sendKeys(arg1);
            delayFor(200);
        }

        @When("^User click Com Name \"([^\"]*)\" as CompanyName optinal$")
        public void user_click_Com_Name_as_CompanyName_optinal (String arg1) throws Throwable {
            WebElement element = driver.findElement(By.xpath("//*[@id='companyName']"));
            highlight(element);
            element.sendKeys(arg1);
            delayFor(200);
        }

        @Then("^User click check address can't be P\\.O\\.Box$")
        public void user_click_check_address_can_t_be_P_O_Box () throws Throwable {
            WebElement element = driver.findElement(By.xpath("//div[10]/div/label/span"));
            highlight(element);
            jsClickEx(element);
            //  element.click();
        }

        @When("^User click save for continue$")
        public void user_click_save_for_continue () throws Throwable {
            WebElement element = driver.findElement(By.xpath("//div[10]/div/label/div/span"));
            highlight(element);
            element.click();
            delayFor(200);
        }

    @Then("^user get address Verifacdtion page$")
    public void user_get_address_Verifacdtion_page() throws Throwable {
      // WebElement element = driver.findElement(By.xpath("//title"));
        String actualTitle = driver.getTitle();
        String expectedTitle = "B&N | Manage Address Book";
        assertEquals(expectedTitle,actualTitle);

      /* String actual = driver.getTitle();
        Assert.assertEquals("B&N | Manage Address Book",actual);*/

      }

    }

