package com.shiftedtech.barnesandnoble.steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BeforeAndAfterHook extends StepBase {
    private static final int DEFAULT_IMPLICITY_WAIT_TIME = 30;

    @Before
    public void before(){
        ChromeDriverManager.getInstance().arch32().setup();
        driver = new ChromeDriver();
       // driver.get("https://www.barnesandnoble.com/");

         // FirefoxDriverManager.getInstance().setup();
         // driver = new FirefoxDriver();
        System.out.println("Statement just before implicit wait" + new Timestamp(new Date().getTime()));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        System.out.println("Statement just after implicit wait" + new Timestamp(new Date().getTime()));
        driver.manage().window().maximize();
    }

    @After
    public void after(){
        try {
            driver.close();
            driver.quit();
            driver = null;
        }
        catch (Exception ex){
    }   }
}
